// Introduction to JS OOP


// Object
	// data structure containing collection of properties that have a name or key and value


	// Global/PreDefined Objects in JS
	// Arrays (special type of obj)
		// Has properties and methods for traversing and manipulating collections of data
	// Math
		// Has properties and methods for mathematical constants and functions

// alert("Hello")

let students = ["John", "Joe", "Jane", "Jessie"];
console.log(students)


// Array Review:
		// 1. PUSH - Add item at end of array

		students.push("Kris");
		console.log(students)


		// 2. UNSHIFT - Add item at beginning of array
		students.unshift("Eronna")
		console.log(students)


		// 3. POP - Remove item at end of array
		students.pop()
		console.log(students)

		// 4. SHIFT - Remove item at beginning of array
		students.shift()
		console.log(students)



		// SLICE
			// Non Mutator
			// 
		// SPLICE
			// Mutator


		// Mini Activity

			// Using forEach, check if every item in the array is divisible by 5. If they are, log in the console "<num>" is divisible by 5, if not, log false


		// Check if a nuumber is divisible by #
			// use Modulo %

			let arrNum = [15,20,23,30,37];

			arrNum.forEach(num =>{
				if (num % 5 == 0){
					console.log(`${num} is divisible by 5`)
				} else {
					console.log(false)
				}
			})



// Math Object
// Javascript Math Object - allows us to perform mathematical tasks on numbers.
// All methods and properties can be used without creating a Math object first.

// Mathematical Constants
	// 8 Predefined properties which can be called via the syntax Math.property

			/*Syntax
				Math.property
	
			*/


	console.log(Math)
	// Examples of Mathematical Constants
		console.log(Math.E)
		console.log(Math.PI)
		console.log(Math.SQRT2)
		console.log(Math.SQRT1_2)
		console.log(Math.LN2)
		console.log(Math.LN10)
		console.log(Math.LOG2E)
		console.log(Math.LOG10E)

	// Example of Methods
		// Round OF
		console.log(Math.round(Math.PI))
		/*Ex: 3.9
		round 4 
		floor 3*/

		// Round UP
		console.log(Math.ceil(Math.PI))

		// Round FLOOR
		console.log(Math.floor(Math.PI))

		// TRUNC -ES6 UPDATE
		console.log(Math.trunc(Math.PI))



	// Square Root of a  number
		console.log(Math.sqrt(3))


let numVar = -100;
	// lowest value in a list of arguments
		console.log(Math.min(-1,-2,-4,0,1,2,-3, numVar))
		// -100


	// highest value in a list of arguments
		console.log(Math.max(-1,-2,-4,0,1,2,-3, numVar))
		// 2

// Exponent Operator
	// 1. **
	const myNumber = 8**5;
	console.log(myNumber)

	// 2. Math.pow()
	const myOtherNumber = Math.pow(8,5);
	console.log(myOtherNumber)




// Objects vs Primitive Data Types

	// string
	// number
	// boolean
	// undefined
	// null


let name = "Mario"
console.log(name)


